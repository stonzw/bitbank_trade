import json
import hmac
import hashlib
import requests
import urllib.request, urllib.parse, urllib.error
import numpy as np
import random
import time
import math
from datetime import datetime
from loguru import logger
from Trader import Trader
from Directer import Directer
import sys
import os
import os.path as osp
sys.path.append(os.path.join(os.path.dirname(__file__), "designpy"))
from Repeater import Repeater
import redis
import pandas as pd
import decimal
from logging import getLogger, StreamHandler, FileHandler, Formatter,DEBUG,ERROR
log_file_path = osp.join(osp.dirname(__file__), "log/trading_{time}.log")
logger.add(log_file_path)

class RaterBitbankBybit(object):
    def __init__(self, bybit_mean_window_size, sigma):
        self.bybit_mean_window_size = bybit_mean_window_size
        self.sigma = sigma
        self.logger = None
        self.amount_unit = decimal.Decimal("0.001")
    def reload_status(self):
        bybit_trade_history = read_bybit_table()
        bitbank_trade_history = read_bitbank_table()
        self.bybit_average_rate = bybit_trade_history[time.time() - self.bybit_mean_window_size < bybit_trade_history.timestamp].price.mean()
        self.bitbank_average_rate = bitbank_trade_history[time.time() - self.bybit_mean_window_size < bitbank_trade_history.timestamp].price.mean()
        self.bitbank_latest_rate = bitbank_trade_history.price.values[-1]
        self.bybit_latest_rate = bybit_trade_history.price.values[-1]
    def is_any_values_nan(self):
        return np.isnan([
            self.bybit_latest_rate,
            self.bybit_average_rate,
            self.bitbank_average_rate
        ]).any()
    def get_buy_rate(self):
        if not self.is_any_values_nan():
            buy_rate = int(self.bybit_latest_rate * (self.bitbank_average_rate / self.bybit_average_rate - self.sigma))
            return buy_rate
    def get_sell_rate(self):
        if not self.is_any_values_nan():
            sell_rate = int(self.bybit_latest_rate * (self.bitbank_average_rate / self.bybit_average_rate + self.sigma))
            return sell_rate

def read_json(fname):
    with open(fname) as f:
        return json.load(f)
        
        
client = redis.Redis(host='localhost', port=6379, db=0)

def read_bybit_table():
    bybit_trade_history = pd.DataFrame(json.loads(client.get("bybit_recent_trades").decode('utf-8')))[["price", "trade_time_ms"]]
    bybit_trade_history["timestamp"] = bybit_trade_history.trade_time_ms / 1000
    return bybit_trade_history[["price", "timestamp"]]

def read_bitbank_table():
    bitbank_trade_history = pd.DataFrame(json.loads(client.get("bitbank_recent_trades").decode('utf-8')))
    bitbank_trade_history["timestamp"] = bitbank_trade_history.executed_at / 1000
    return bitbank_trade_history[["price", "timestamp"]].astype(np.float64)

class Trading(Repeater):
    def __init__(self,pair, buy_flag, sell_flag):
        self.handler_format = Formatter('%(asctime)s - %(levelname)s - %(message)s')
        self.logger = getLogger(__name__)
        self.logger.setLevel(DEBUG)
        self.setErrorFileLogger(osp.join(osp.dirname(__file__), "error.log"))
        self.rater = RaterBitbankBybit(30, 0.1)
        self.times=-1
        filename = osp.join(osp.dirname(__file__), "config/bitbank_secret.json")
        keys = read_json(filename)
        self.directer = Directer(keys["director_access"], keys["director_secret"])
        self.trader = Trader(keys["trader_access"],keys["trader_secret"])
        self.buy_flag = buy_flag
        self.sell_flag = sell_flag
        self.pair = pair
        self.count = 0
        if pair=="btc_jpy":
            self.getCoin = self.directer.getMyBtc
            self.sellCoin = lambda x:self.trader.sellBtc(x[0],x[1])
            self.buyCoin = lambda x:self.trader.buyBtc(x[0],x[1])
            self.minOrderCoinAmount = 0.001
            self.maxOrderCoinAmount = decimal.Decimal("0.5")
            self.minOrderJpyAmount = 6000
            self.maxOrderJpyAmount = 3000000

    def main(self):
        if self.count % 10 == 0:
            self.directer.reloadMyWallet()
            time.sleep(2)
        self.rater.reload_status()
        ordered = False
        if self.minOrderJpyAmount < self.directer.getMyJpy() and self.buy_flag:
            buy_rate = self.rater.get_buy_rate()
            if buy_rate:
                buy_amount = int((self.directer.getMyJpy()/buy_rate)/float(self.rater.amount_unit)) * self.rater.amount_unit
                buy_amount = min(self.maxOrderCoinAmount,buy_amount)
                self.buyCoin((buy_rate,str(buy_amount)))
            else:
                self.logger.warning("buy_rate is None")
        if self.minOrderCoinAmount < self.getCoin() and self.sell_flag:
            sell_rate = self.rater.get_sell_rate()
            if sell_rate:
                sell_amount =  min(self.maxOrderCoinAmount,self.getCoin())
                self.sellCoin((sell_rate,sell_amount))
            else:
                self.logger.warning("sell_rate is None")
        time.sleep(3)
        order_ids = self.directer.get_active_orders(self.pair)
        if order_ids:
            self.trader.cancel_orders(self.pair,order_ids)
        self.count += 1
    def errorHandler(self,e):
        if type(e) == IndexError:
            time.sleep(5)
        elif type(e) == AssertionError:
            time.sleep(5)
        else:
            self.defaultErrorHandler(e)
            time.sleep(20)


if __name__ == '__main__':
    filename = osp.join(osp.dirname(__file__), "config/config.json")
    config = read_json(filename)
    if config["side"] == 'buy':
        trading = Trading("btc_jpy", True, False)
    elif config["side"] == 'sell':
        trading = Trading("btc_jpy", False, True)
    elif config["side"] == "both":
        trading = Trading("btc_jpy", True, True)
    else:
        raise NotImplemented
    trading.logger = logger
    trading.start()
