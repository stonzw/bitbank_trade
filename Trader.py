from bitbankApi import bitbankcc_private
class Trader(bitbankcc_private):
    def __init__(self, api_key, api_secret):
        super(Trader,self).__init__(api_key, api_secret)
        self.res = None
    def __trade(self, rate, amount, choice, pair):
        self.order(pair, rate, amount, choice, "limit")
    def __tradeJpyBtc(self, rate, amount, choice):
        self.__trade(rate, amount, choice, "btc_jpy")
    def __tradeJpyXrp(self, rate, amount, choice):
        self.__trade(rate, amount, choice, "xrp_jpy")
    def __tradeJpyBcc(self, rate, amount, choice):
        self.__trade(rate, amount, choice, "bcc_jpy")
    def buyBtc(self, rate, amount):
        self.__tradeJpyBtc(rate, amount, "buy")
    def sellBtc(self, rate, amount):
        self.__tradeJpyBtc(rate, amount, "sell")
    def buyXrp(self, rate,amount):
        self.__tradeJpyXrp(rate, amount, "buy")
    def sellXrp(self, rate,amount):
        self.__tradeJpyXrp(rate, amount, "sell")
    def buyBcc(self, rate,amount):
        self.__tradeJpyBcc(rate, amount, "buy")
    def sellBcc(self, rate,amount):
        self.__tradeJpyBcc(rate, amount, "sell")
