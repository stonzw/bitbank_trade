from bitbankApi import bitbankcc_public
import numpy as np
import json
import os
import time
class Rater(bitbankcc_public):
    def __init__(self, pair, thres = None):
        super(Rater,self).__init__()
        self.res = None
        self.baseRate = 0
        self.local = {}
        self.pair = pair
        self.board = []
        if self.pair == "btc_jpy":
            if thres:
                self.thresPercent = thres
            else:
                self.thresPercent = 0.3
            self.amount_unit = 0.001
            self.rate_unit = 1
            self.thres_abs = 5000
            self.bincount = 40
            self.few_rate = 1
        elif self.pair == "xrp_jpy":
            if thres:
                self.thresPercent = thres
            else:
                self.thresPercent = 0.4
            self.amount_unit = 1
            self.rate_unit = 0.001
            self.thres_abs = 3.5
            self.bincount = 20
            self.few_rate = 0.001
        elif self.pair == "bcc_jpy":
            if thres:
                self.thresPercent = thres
            else:
                self.thresPercent = 0.4
            self.amount_unit = 0.001
            self.rate_unit = 1
            self.thres_abs = 5000
            self.bincount = 20
            self.few_rate = 1
    def selectIdx(self, histVal):
        thres = self.thresPercent * histVal[0]
        histDiff = np.diff(histVal)
        return (np.where(thres < histDiff)[0][0] + 1)
    def getOrderRate(self, rateType):
        asks = [[float(x[0]), float(x[1])] for x in self.res["data"]["asks"]]
        bids = [[float(x[0]), float(x[1])] for x in self.res["data"]["bids"]]
        hist = self.cnvrtRawBoard2Hist({"asks":asks, "bids":bids}, rateType)
        histVal = hist[0]
        histLabel = hist[1]
        targetRateIdx = self.selectIdx(histVal)
        binWidth = np.diff(histLabel)[0]
        if rateType == "bids":
            fewYen = -self.few_rate
        elif rateType == "asks":
            fewYen = self.few_rate
        return int((histLabel[targetRateIdx] - fewYen)/self.rate_unit) * self.rate_unit
    def cnvrtOrder2List(self, rawOrder):
        if abs(self.baseRate - float(rawOrder[0]))< self.thres_abs:
            return [float(rawOrder[0])] * int(float(rawOrder[1])/self.amount_unit)
        else:
            return []
    def cvnvrtBoard2Hist(self, board, rateType):
        raw_hist = np.histogram(board, bins=self.bincount)
        if rateType == "bids":
            return (raw_hist[0][::-1], raw_hist[1][::-1])
        else:
            return raw_hist
    def cnvrtRawBoard2Hist(self, raw, rateType):
        if rateType == "bids":
            baseRate = float(raw[rateType][0][0])
            self.baseRate = baseRate
        else:
            baseRate = float(raw[rateType][0][0])
            self.baseRate = baseRate
        board = np.concatenate(list(map(self.cnvrtOrder2List, raw[rateType])))
        self.board = board
        hist = self.cvnvrtBoard2Hist(board, rateType)
        self.hist = hist
        return hist
    def checkError(self):
        if not self.res["success"]:
            print("Instance " + self.name + " raised Error.")
            print("Error is " + self.res["error"])
            return True
    def reloadBoard(self, is_save=True):
        self.board_data_dir = "data/board/%s"%str(int(time.time()/3600))
        if not os.path.exists(self.board_data_dir):
            os.makedirs(self.board_data_dir)
        self.res = self.get_depth(self.pair)
        if is_save:
            with open(os.path.join(self.board_data_dir,"%s.json"%int(time.time())),"w") as f:
                json.dump(self.res,f)
    def getBuyRate(self):
        return self.getOrderRate("bids")
    def getSellRate(self):
        return self.getOrderRate("asks")
    def getIceSellRate(self):
        return float(self.res["data"]["asks"][0][0]) - 0.001
    def getIceBuyRate(self):
        return float(self.res["data"]["bids"][0][0]) + 0.001
