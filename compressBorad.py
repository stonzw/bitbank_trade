import shutil
import tarfile
import os
import time
import sys

def make_tarfile(output, src):
  with tarfile.open(output, "w:gz") as tar:
    tar.add(src, arcname=os.path.basename(src))
  shutil.rmtree(src)
root_dir = os.path.dirname(os.path.abspath(__file__))
output_dir = os.path.join(root_dir,"board_tar_gz/")
if not os.path.exists(output_dir):
  os.makedirs(output_dir)
hour_time = 60 * 60
ago = 200
now = int(time.time()/hour_time)
target_time = range(now-ago,now)
for i in target_time:
  src_file = os.path.join(root_dir,"data/board/%s"%i)
  if os.path.exists(src_file):
    make_tarfile(os.path.join(output_dir,"%s.tar.gz"%i),src_file)
