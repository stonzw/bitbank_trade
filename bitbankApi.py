#coding: utf-8
import os
import sys
import json
import hmac
import hashlib
import urllib.request, urllib.parse, urllib.error
import time
import math
from logging import getLogger, StreamHandler, FileHandler, Formatter,DEBUG,ERROR
sys.path.append(os.path.expanduser("~/designpy"))
from utils import error_parser
from ApiClient import ApiClient
def sign_request(key, query):
    h = hmac.new(bytearray(key, 'utf8'), bytearray(query, 'utf8'), hashlib.sha256)
    return h.hexdigest()

def make_header(query_data, api_key, api_secret):
    nonce = str(int(time.time() * 1000))
    message = nonce + query_data
    return {
        'Content-Type': 'application/json',
        'ACCESS-KEY': api_key,
        'ACCESS-NONCE': nonce,
        'ACCESS-SIGNATURE': sign_request(api_secret, message)
    }
class bitbankcc_public(ApiClient):
    
    def __init__(self):
        super(bitbankcc_public,self).__init__('https://public.bitbank.cc')
    
    def get_ticker(self, pair):
        path = '/' + pair + '/ticker'
        return error_parser(self._get(path))
    
    def get_depth(self, pair):
        path = '/' + pair + '/depth'
        return error_parser(self._get(path))
    
    def get_transactions(self, pair, yyyymmdd=None):
        path = '/' + pair + '/transactions'
        if yyyymmdd: path += '/' + yyyymmdd
        return error_parser(self._get(path))
    
    def get_candlestick(self, pair, candle_type, yyyymmdd):
        path = '/' + pair + '/candlestick/' + candle_type + '/' + yyyymmdd
        return error_parser(self._get(path))

class bitbankcc_private(ApiClient):
    
    def __init__(self, api_key, api_secret):
        super(bitbankcc_private,self).__init__('https://api.bitbank.cc')
        self.api_key = api_key
        self.api_secret = api_secret
    
    def _get(self, path, query):
        data = path + urllib.parse.urlencode(query)
        headers = make_header(data, self.api_key, self.api_secret)
        response = super(bitbankcc_private,self)._get(path,query,headers=headers)
        return error_parser(response)
    
    def _post(self, path, query):
        data = json.dumps(query)
        headers = make_header(data, self.api_key, self.api_secret)
        response = super(bitbankcc_private,self)._post(path,query,headers=headers)
        return error_parser(response)
    
    def get_asset(self):
        return self._get('/v1/user/assets', {})
    
    def get_order(self, pair, order_id):
        return self._get('/v1/user/spot/order?', {
            'pair': pair,
            'order_id': order_id
        })
    
    def get_active_orders(self, pair, options=None):
        if options is None:
            options = {}
        if not 'pair' in options:
            options['pair'] = pair
        return self._get('/v1/user/spot/active_orders?', options)

    def order(self, pair, price, amount, side, order_type):
        return self._post('/v1/user/spot/order', {
            'pair': pair,
            'price': price,
            'amount': amount,
            'side': side,
            'type': order_type
        })
    
    def cancel_order(self, pair, order_id):
        return self._post('/v1/user/spot/cancel_order', {
            'pair': pair,
            'order_id': order_id
        })

    def cancel_orders(self, pair, order_ids):
        return self._post('/v1/user/spot/cancel_orders', {
            'pair': pair,
            'order_ids': order_ids
        })

    def get_orders_info(self, pair, order_ids):
        return self._post('/v1/user/spot/orders_info', {
            'pair': pair,
            'order_ids': order_ids
        })

    def get_trade_history(self, pair, order_count):
        return self._get('/v1/user/spot/trade_history?', {
            'pair': pair,
            'count': order_count
        })

    def get_withdraw_account(self, asset):
        return self._get('/v1/user/withdrawal_account?', {
            'asset': asset
        })

    def request_withdraw(self, asset, uuid, amount, token):
        q = {
            'asset': asset,
            'uuid': uuid,
            'amount': amount
        }
        q.update(token)
        return self._post('/v1/user/request_withdrawal', q)
