from bitbankApi import bitbankcc_private
class Directer(bitbankcc_private, object):
    def __init__(self, access_key, secret_key):
        super(Directer, self).__init__(access_key, secret_key)
        self.traders = []
        self.myWallet = {}
    def reloadMyWallet(self):
        res = self.get_asset()
        for asset in res['data']['assets']:
            self.myWallet[str(asset["asset"])] = float(asset["free_amount"])
    def append(self, trader):
        self.traders.append(trader)
    def getMyJpy(self):
        return float(self.myWallet["jpy"])
    def getMyBtc(self):
        return float(self.myWallet["btc"])
    def getMyXrp(self):
        return float(self.myWallet["xrp"])
    def getMyBcc(self):
        return float(self.myWallet["bcc"])
    def get_active_orders(self, pair, options=None):
        orders = super(Directer, self).get_active_orders(pair)["data"]["orders"]
        order_ids = []
        for order in orders:
            order_id = order["order_id"]
            order_ids.append(order_id)
        return order_ids
