import sys
import os
import time
from Trader import Trader
from Directer import Directer
from Rater import Rater
from datetime import datetime
sys.path.append(os.path.expanduser(r"~/designpy"))
from Repeater import Repeater
sys.path.append(os.path.expanduser(r"~/karakuri"))
from getpass import getpass
from yomu_json import read_json
class Recording(Repeater):
    def __init__(self):
        super().__init__()
        self.raterBtc = Rater(pair="btc_jpy")
        self.raterXrp = Rater(pair="xrp_jpy")
        enc_file_name="bitbank.json"
        ps = getpass("password please:")
        keys = read_json(enc_file_name,ps.encode("utf-8"))
        self.directer = Directer(keys["director_access"], keys["director_secret"])
    def main(self):
        self.directer.reloadMyWallet()
        time.sleep(2)
        self.raterBtc.reloadBoard(False)
        time.sleep(2)
        self.raterXrp.reloadBoard(False)
        btc_rate = float(self.raterBtc.res["data"]["asks"][0][0])
        xrp_rate = float(self.raterXrp.res["data"]["asks"][0][0])
        jpy = self.directer.getMyJpy()
        xrp = self.directer.getMyXrp()
        btc = self.directer.getMyBtc()
        asset_jpy = jpy + xrp * xrp_rate + btc * btc_rate
        asset_xrp = asset_jpy / xrp_rate
        asset_btc = asset_jpy / btc_rate
        time_stamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        line = "%s,%s,%s,%s,%s,%s,%s\n" % (time_stamp,asset_jpy,asset_xrp,asset_btc,jpy,xrp,btc)
        with open("result_bitbank.csv", "a") as f:
             f.write(line)
        time.sleep(3600)
    def errorHandler(self,e):
        print(e)
        time.sleep(5)
if __name__ == '__main__':
    recording = Recording()
    recording.start()
